
/*Первый семинар.
1. Выбросить случайное целое число в диапазоне от 0 до 2000 и сохранить в i
2. Посчитать и сохранить в n номер старшего значащего бита выпавшего числа
3. Найти все кратные n числа в диапазоне от i до Short.MAX_VALUE сохранить в массив m1
4. Найти все некратные n числа в диапазоне от Short.MIN_VALUE до i и сохранить в массив m2

Пункты реализовать в методе main
*Пункты реализовать в разных методах

int i = new Random().nextInt(k); //это кидалка случайных чисел!)

import java.util.Random;

public class fail {
    public static void main(String[] args){
        Random random = new Random();
        int i = random.nextInt(2000);
        System.out.println("Случайное число i =  " + i);

        int n = Integer.toBinaryString(i).length()-1;
        System.out.println("Номер старшего значения бита n =  "+n);
          
        short count = 0;
        for (int j = i; j < Short.MAX_VALUE; j++){
            if ( j % n == 0) {
                count++;
            }
        }
        System.out.println("массив m1 =  "+ count); 
        int[] m1 = new int [count];
        count = 0;
               
        for (int j = i; j < Short.MAX_VALUE; j++){
            if (j % n == 0) {
                m1[count++] = j;
                }
        } 

        count = 0;
        
        for (int j = Short.MIN_VALUE; j < i; j++){
            if ( j % n != 0) {
                count++;
            }
        }
        System.out.println("массив m2 =  "+ count); 
        int[] m2 = new int [count];
        count = 0;
                
        for (int j = Short.MIN_VALUE; j < i; j++){
            if (j % n != 0) {
                m2[count++] = j;
                }
        }       

    }
}*/

// task sem6

import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class fail {
    
    public static void main(String[] args) {
        
        MySet<Integer> numbers = new MySet<>();

        for (int i = 0; i < 100; i++) {
            numbers.addNum(new Random().nextInt(100));
        }

        Iterator <Integer> iter = numbers.iterator();
        while (iter.hasNext()) {
            int a = iter.next();
            if (a % 2 == 0){
                System.out.println(a);
            }
        }
        System.out.println(numbers);
    }
}

class MySet<T> {

    private HashMap<T, Object> myMap = new HashMap<>();
    private static final Object OBJECT = new Object();

    public void addNum(T i) {

        myMap.put(i, OBJECT);

    }

    public boolean remove(T i) {

        return myMap.remove(i) == OBJECT;
    }

    public boolean contains(T i) {

        return myMap.containsKey(i);
    }

    public String toString() {

        StringBuilder str = new StringBuilder("[");
        Iterator<T> iter = myMap.keySet().iterator();

        while (iter.hasNext()){
            str.append(iter.next() + ",");
        }
        str.append("]");

        return str.toString();
    }

    public Iterator <T> iterator(){
        return myMap.keySet().iterator();
    }  
}